import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { MovieviewComponent } from "./components/movieview/movieview.component";

const routes: Routes = [
  { path: '', loadChildren: './components/tabs/tabs.module#TabsPageModule' },
  { path: "movieview/:id", component: MovieviewComponent }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
