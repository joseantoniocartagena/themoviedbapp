import { Routes } from "@angular/router";
import { MovieviewComponent } from "./components/movieview/movieview.component";

export const ROUTES: Routes = [
    { path: '', loadChildren: './components/tabs/tabs.module#TabsPageModule' },
    { path: "movieview/:id", component: MovieviewComponent }
];