import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviedbService } from "../../services/moviedb.service";

@Component({
  selector: 'app-movieview',
  templateUrl: './movieview.component.html',
  styleUrls: ['./movieview.component.scss'],
})
export class MovieviewComponent {

  // pelicula: any;
  pelicula: any = {};
  posterUrl = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
  loadingPelicula: boolean;
  constructor(private router: ActivatedRoute,
    private moviedb: MoviedbService) {

    this.loadingPelicula = true;

    this.router.params.subscribe(params => {
      console.log(params);

      this.moviedb.getPelicula(params['id'])
        .subscribe(pelicula => {
          this.pelicula = pelicula;
          this.loadingPelicula = false;
        })
    })

  }

}
