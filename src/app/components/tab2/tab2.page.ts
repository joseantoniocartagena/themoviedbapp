import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { MoviedbService } from '../../services/moviedb.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  items: any[] = [];
  posterUrl = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
  constructor(private moviedb: MoviedbService, private router: Router) {}
  ionViewWillEnter(){
    this.moviedb.getDiscoverMovies()
      .subscribe((data: any) => {

        console.log(data);
        this.items = data;
      });
  }
  verPelicula(item: any) {

    let peliculaId;

    peliculaId = item.id;

    this.router.navigate(["/movieview", peliculaId]);
  }

}
