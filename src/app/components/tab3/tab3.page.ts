import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MoviedbService } from "../../services/moviedb.service";

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  items: any[] = [];
  posterUrl = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
  loading: boolean;

  constructor(private moviedb: MoviedbService, private router: Router) { }

  buscar(termino: string) {
    console.log(termino);

    this.loading = true;
    this.moviedb.getBusquedaPeliculas(termino).subscribe((data: any) => {
      console.log(data);

      this.items = data;
      this.loading = false;
    });
  }
  verPelicula(item: any) {

    let peliculaId;

    peliculaId = item.id;

    this.router.navigate(["/movieview", peliculaId]);
  }

}
